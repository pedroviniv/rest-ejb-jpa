/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.persistence.impl;

import com.mycompany.jpa.multiple.pus.persistence.interfaces.PessoaRepository;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.mycompany.jpa.multiple.pus.persistence.factories.RepositorioCidade;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Stateless
@Local(PessoaRepository.class)
@RepositorioCidade("bayeux")
public class PessoaJpaRepositoryBayeux extends PessoaJpaRepository {
    
    @PersistenceContext(unitName = "pessoa-pu-1")
    private EntityManager manager;

    @Override
    protected EntityManager getEntityManager() {
        return this.manager;
    }
}
