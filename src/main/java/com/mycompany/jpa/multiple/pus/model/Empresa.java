/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Entity
public class Empresa implements Serializable {
    
    @Id 
    @GeneratedValue
    private Long id;
    
    @OneToMany
    @JoinColumn(name = "empresa_id")
    private List<SimplePessoa> pessoas;

    public Empresa() {
        this.pessoas = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SimplePessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<SimplePessoa> pessoas) {
        this.pessoas = pessoas;
    }
    
    public void addPessoa(SimplePessoa pessoa) {
        this.pessoas.add(pessoa);
    }

    @Override
    public String toString() {
        return "Empresa{" + "id=" + id + ", pessoas=" + pessoas + '}';
    }
}
