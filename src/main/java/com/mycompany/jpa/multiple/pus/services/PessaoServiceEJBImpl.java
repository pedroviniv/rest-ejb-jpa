/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa.multiple.pus.services;

import com.mycompany.jpa.multiple.pus.model.Pessoa;
import com.mycompany.jpa.multiple.pus.persistence.factories.FabricaRepositorios;
import com.mycompany.jpa.multiple.pus.persistence.interfaces.PessoaRepository;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Stateless
@Local(PessoaService.class)
public class PessaoServiceEJBImpl implements PessoaService {

    private PessoaRepository pessoaRepository;

    private void injectRepositories(String cidade) {
        this.pessoaRepository = FabricaRepositorios
                .getInstancia(PessoaRepository.class, cidade);
    }

    @Override
    public void persist(Pessoa pessoa, String cidade) {

        this.injectRepositories(cidade);
        this.pessoaRepository.persist(pessoa);
    }

    @Override
    public List<Pessoa> listar(String cidade) {

        this.injectRepositories(cidade);
        return this.pessoaRepository.listAll();
    }

    @Override
    public Pessoa buscarPorId(String cidade, Long pessoaId) {

        this.injectRepositories(cidade);
        return this.pessoaRepository.find(pessoaId);
    }

}
