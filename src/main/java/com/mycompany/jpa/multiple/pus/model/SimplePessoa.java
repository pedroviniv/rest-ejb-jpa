/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Entity
@Table(name = "pessoa")
public class SimplePessoa implements Serializable {
    
    @Id
    private Long id;

    public SimplePessoa(Long id) {
        this.id = id;
    }

    public SimplePessoa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SimplePessoa{" + "id=" + id + '}';
    }
}
