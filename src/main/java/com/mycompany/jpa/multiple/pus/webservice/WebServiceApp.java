/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.webservice;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@javax.ws.rs.ApplicationPath("api/v1")
public class WebServiceApp extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(PessoaWebService.class);
    }
}
