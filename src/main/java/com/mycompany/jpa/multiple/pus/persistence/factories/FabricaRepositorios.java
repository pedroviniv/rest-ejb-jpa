/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa.multiple.pus.persistence.factories;

import com.mycompany.jpa.multiple.pus.persistence.interfaces.PessoaRepository;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.reflections.Reflections;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class FabricaRepositorios {

    private static final Logger LOG = Logger
            .getLogger(FabricaRepositorios.class.getName());

    private static <T> Class<? extends T> primeiraClasseComAnotacao(Set<? extends Class<? extends T>> classes, String city) {
        
        for (Class<? extends T> clazz : classes) {
            if (clazz.isAnnotationPresent(RepositorioCidade.class)) {
                RepositorioCidade annotation = clazz
                        .getAnnotation(RepositorioCidade.class);
                if (annotation.value().equals(city)) {
                    return clazz;
                }
            }
        }
        
        return null;
    }

    /**
     * Recupera uma instância de EJB de acordo com a interface de acesso e 
     * a cidade passada por parâmetro. O fluxo acontece da seguinte maneira:
     * As extensões da interface são buscadas com auxílio da API Reflections.
     * <br/>
     * São filtradas das classes resultantes, as classes que possuem a anotação 
     * {@code @RepositorioCidade}, por fim, a primeira classe cuja anotação 
     * {@code @RepositorioCidade} possui a cidade passada por parâmetro como valor,
     * é feito o lookup desta classe e retornado.
     * @param <T> 
     * @param clazz Interface de acesso
     * @param cidade Cidade na qual deseja se obter a implementação do repositório
     * @return Instância do repositório
     */
    public static <T> T getInstancia(Class<? extends T> clazz, String cidade) {

        String jndiTemplate = "java:global/jpa-multiple-pus/%s!%s";

        Reflections ref = new Reflections("com.mycompany.jpa.multiple.pus.persistence");

        Set<? extends Class<? extends T>> classSubtypes = ref
                .getSubTypesOf(clazz);

        if (classSubtypes.isEmpty()) {
            throw new IllegalArgumentException(clazz.getName() + " doesn't have any implementations!");
        }

        Class<? extends T> cityClassImpl
                = primeiraClasseComAnotacao(classSubtypes, cidade);

        if (cityClassImpl == null) {
            throw new IllegalArgumentException(clazz.getName()
                    + " doesn't have any implementations for the city "
                    + cidade);
        }

        String jndiName = String.format(jndiTemplate, cityClassImpl.getSimpleName(), clazz.getName());

        try {
            return (T) InitialContext.doLookup(jndiName);
        } catch (NamingException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
