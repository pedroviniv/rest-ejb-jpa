/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.persistence.impl;

import com.mycompany.jpa.multiple.pus.persistence.interfaces.JpaRepository;
import javax.persistence.EntityManager;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */


public abstract class JpaGenericRepository implements JpaRepository {
    
    protected abstract EntityManager getEntityManager();
    
    @Override
    public void persist(Object obj) {
        this.getEntityManager().persist(obj);
    }
    
    @Override
    public void remove(Object obj) {
        this.getEntityManager().remove(obj);
    }
}
