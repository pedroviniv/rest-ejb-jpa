/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.persistence.interfaces;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface JpaRepository {

    void persist(Object obj);

    void remove(Object obj);

}
