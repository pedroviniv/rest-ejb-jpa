/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.persistence.impl;

import com.mycompany.jpa.multiple.pus.persistence.interfaces.PessoaRepository;
import com.mycompany.jpa.multiple.pus.model.Pessoa;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

public abstract class PessoaJpaRepository extends JpaGenericRepository implements PessoaRepository {

    public List<Pessoa> listAll() {
        return this.getEntityManager()
                .createQuery("SELECT p FROM Pessoa p", Pessoa.class)
                .getResultList();
    }
    
    public Pessoa find(Long pessoaId) {
        return this.getEntityManager().find(Pessoa.class, pessoaId);
    }

}
