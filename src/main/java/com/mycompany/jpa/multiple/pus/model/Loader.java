/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.model;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class Loader {

    public static void main(String[] args) {
        
        EntityManager entityManager = Persistence
                .createEntityManagerFactory("resource_local_pu")
                .createEntityManager();
        
        Pessoa pessoa = new Pessoa("Pedro Arthur", "111.222.333-44");
        
        entityManager.getTransaction().begin();
        entityManager.persist(pessoa);
        entityManager.getTransaction().commit();
        
        Empresa empresa = new Empresa();
        
        entityManager.getTransaction().begin();
        entityManager.persist(empresa);
        entityManager.getTransaction().commit();
        
        SimplePessoa simplePessoa = new SimplePessoa(pessoa.getId());
        
        empresa.addPessoa(simplePessoa);
        
        entityManager.getTransaction().begin();
        entityManager.merge(empresa);
        entityManager.getTransaction().commit();
        
        
        
        
        
        
    }
}
