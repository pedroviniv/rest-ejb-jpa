/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jpa.multiple.pus.persistence.interfaces;

import com.mycompany.jpa.multiple.pus.model.Pessoa;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface PessoaRepository extends JpaRepository {

    public List<Pessoa> listAll();
    public Pessoa find(Long pessoaId);
}
