/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jpa.multiple.pus.webservice;

import com.mycompany.jpa.multiple.pus.model.Pessoa;
import com.mycompany.jpa.multiple.pus.persistence.factories.FabricaRepositorios;
import com.mycompany.jpa.multiple.pus.persistence.interfaces.PessoaRepository;
import com.mycompany.jpa.multiple.pus.services.PessoaService;
import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Path("pessoas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PessoaWebService {

    @EJB
    private PessoaService pessoaService;
    private static final Logger LOG = Logger.getLogger(PessoaWebService.class.getName());
    
    @POST
    @Path("{cidade}")
    public Response novaPessoa(
            @PathParam("cidade") String cidade, 
            Pessoa pessoa, @Context UriInfo uriInfo) {

        LOG.log(Level.INFO, "pessoa payload: {0}", pessoa);
        
        this.pessoaService.persist(pessoa, cidade);
        
        URI uri = uriInfo.getBaseUriBuilder().path(this.getClass())
                .path(String.valueOf(pessoa.getId()))
                .path(cidade)
                .build();

        return Response.created(uri).build();
    }

    @GET
    @Path("{cidade}/{id}")
    public Response findById(@PathParam("cidade") String cidade, 
            @PathParam("pessoaId") @DefaultValue("-1") Long id) {
        
        
       
        Pessoa pessoa = this.pessoaService.buscarPorId(cidade, id);
        return Response.ok(pessoa).build();
    }

    @GET
    @Path("{cidade}")
    public Response listAll(@PathParam("cidade") String cidade) {

        List<Pessoa> pessoas = this.pessoaService.listar(cidade);
        
        LOG.log(Level.INFO, "pessoas: {0}", pessoas);
        
        GenericEntity<List<Pessoa>> pessoasResponse
                = new GenericEntity<List<Pessoa>>(pessoas) {
        };

        return Response.ok(pessoasResponse).build();
    }
}
